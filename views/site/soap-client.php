<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;

$this->title = 'SOAP-клиент';
$this->params['breadcrumbs'][] = $this->title;


?>
    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(['id' => 'soap-form']); ?>

            <?= $form->field($model, 'city')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'name') ?>
            <div class="form-group">
                <?php
                echo '<label>Введите дату</label>';
                echo DatePicker::widget([
                    'name' => 'check_issue_date',
                    'value' => date('Y-m-d', time()),
                    'options' => ['placeholder' => 'Выберите дату ...', 'id' => 'calculatorform-date'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]);
                ?>
            </div>
            <div class="form-group">
                <input id="man" type="checkbox" name="serv1">Мужчина</input>
            </div>
            <div class="form-group">
                <select id="pay_type" name="hero">
                    <option disabled>Выберите тип оплаты</option>
                    <option value="1" selected>PayU</option>
                    <option value="2">Яндекс-Касса</option>
                </select>
            </div>

            <div class="form-group">
                <select id="product" multiple>
                    <option disabled>Выберите товары</option>
                    <option>Компьютер</option>
                    <option>Стол</option>
                </select>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <p><b>Результат:</b></p>
            <div id="error" class="text-danger"><label></label></div>
            <div id="price" class="text-success"><label></label></div>
            <div id="info" class="text-success"><label></label></div>
        </div>
    </div>

<?php
$baseUrl = Url::toRoute('/my/getCalculate');
$script = <<<JS
$(document).on("beforeSubmit", "#soap-form", function () {
     var xmlhttp = new XMLHttpRequest();
            xmlhttp.open('POST', 'http://myservice.test/index.php?r=my%2Fcalculate', true);
            str = '';
            $("#product :selected").each(function(el){
                str = str +  '<product>' +  $(this ).val() + '</product>';
            })

            
            
            // build SOAP request
            var sr =
                '<?xml version="1.0" encoding="utf-8"?>' +
                '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
                    '<soap:Body>' +
                    '<Calculate xmlns="/soap.xml">' +
                            '<city>' + $('#calculatorform-city').val() +'</city>' +
                            '<name>' + $('#calculatorform-name').val() +'</name>' +
                            '<date>' + $('#calculatorform-date').val() +'</date>' +
                            '<man>' + $('#man').is(':checked') +'</man>' +
                            '<pay>' + $( "#pay_type  :selected" ).val() +'</pay>' +
                            '<products>'+  str +'</products>' +
                    '</Calculate>'+
                    '</soap:Body>' +
                '</soap:Envelope>';            
            
            
            $.ajax({
            url: '/index.php?r=my/calculate&ws=1', 
            type: "POST",
            data: sr, 
            contentType: "text/xml",
            dataType: "text",
            success: function(data) {
                var xml = $(data);
                items = xml.find('item');
               
                if (items[0].children[0].textContent != 'error'){
                    price_value = items[0].children[1].textContent;
                    $("#price label").text('Цена: ' + price_value);
                    
                    info_value = items[1].children[1].textContent;
                    $("#info label").text('Информация: ' + info_value);
                    $("#error label").text('');
                }
                else {
                    error = items[0].children[1].textContent;
                     $("#error label").text(error);
                     $("#price label").text('');
                     $("#info label").text('');
                }                
            },
            error: function(data2) {
                successmessage = 'В ходе выполнения запроса произошла ошибка.';
                $("#error label").text(successmessage);
            },
            });
            
            return false;
            });
JS;

$this->registerJs($script, yii\web\View::POS_READY);


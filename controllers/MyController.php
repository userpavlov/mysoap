<?php

namespace app\controllers;


use yii\web\Controller;
use yii\filters\AccessControl;

class MyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'calculate' => [
                'class' => 'mongosoft\soapserver\Action',
                'serviceOptions' => [
                    'disableWsdlMode' => true,
                ]
            ]
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Функция расчета данных, переданных для сервера
     * @param string $city Город
     * @param string $name Имя
     * @param string $date Дата
     * @param boolean $man Мужчина?
     * @param int $pay Система оплаты
     * @param array $products Продукты
     * @soap
     */
    public function Calculate($city, $name, $date, $man, $pay, $products )
    {
        if(\Yii::$app->user->isGuest){
            return "Пользователь не авторизован";
        }

        $now_date = date("Y-m-d");

        if($date < $now_date){
            $data = array('error' => 'Дата меньше сегодняшней даты.');
        }
        else{
            $data = array('price' =>  rand(), 'info' => substr(md5(mt_rand()), 0, 10));
        }

        return $data;
    }
}
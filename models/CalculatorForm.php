<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CalculatorForm extends Model
{
    public $name;
    public $city;
    public $date;




    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'city'], 'required'],
            [['name', 'city'], 'string'],
            // email has to be a valid email address
            ['date', 'date'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'city' => 'Город',
            'date' => 'Дата',
        ];
    }


}
